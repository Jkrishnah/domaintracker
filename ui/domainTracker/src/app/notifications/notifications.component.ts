import { Component, OnInit } from '@angular/core';
import { ApiService } from './../api.service';
import { HttpClient } from '@angular/common/http';
import { ToastrManager } from 'ng6-toastr-notifications';
import { VirtualTimeScheduler } from 'rxjs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  domainData:any=[]

  constructor(private apiService: ApiService,private httpClient: HttpClient,public toastr: ToastrManager) { }
  ngOnInit() {
    this.apiService.getdata().subscribe(data => {
      this.domainData=data;
      console.log(this.domainData)    }, err => {
      console.log("error");
    });
  }
  check_mail(){
    this.apiService.SetdomainData(this.domainData).subscribe(data => {
      this.toastr.successToastr('Email Updated', 'Success!');
      setTimeout(function(){ 
        location.reload(); 
      }, 500);
        }, err => {
          this.toastr.errorToastr('Update Failed', 'Oops!');
    });
}

}
