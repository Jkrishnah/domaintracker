import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient ) { }
  private fetchDataEndPoint: any = '/fetchData';
  private getNewDomain: any = '/new_domian';
  private checkemail: any ='/check_email';
  private getComponentslist: any = '/api/components';
  public SERVER_URL: any = 'http://127.0.0.1:5000';
  
  
  
  public getdata(): Observable<any> {

    return this.httpClient.get(this.SERVER_URL + this.fetchDataEndPoint);
    
  }
  public getNewDomainData(domainName: string): Observable<any> {
    const email=[]
    const domain_data = {domainName,email};
    return this.httpClient.post(this.SERVER_URL + this.getNewDomain, domain_data);
    // return of(this.checkNameMock);
}

public SetdomainData(domainData): Observable<any> {
  console.log(domainData)
  return this.httpClient.post(this.SERVER_URL + this.checkemail,domainData);
  // return of(this.checkNameMock);
}








}
