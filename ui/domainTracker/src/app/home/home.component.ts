import { Component, OnInit } from '@angular/core';
import { ApiService } from './../api.service';
import { HttpClient } from '@angular/common/http';
import { ToastrManager } from 'ng6-toastr-notifications';
import { VirtualTimeScheduler } from 'rxjs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  domainData:any=[]
  newDomainData:any={}
  newdomainTrigger=false
  newDomainName:any=''
  
addRow() {    
this.newdomainTrigger=true;

  } 
  
  constructor(private apiService: ApiService,private httpClient: HttpClient,public toastr: ToastrManager) { }

  ngOnInit() {
    this.apiService.getdata().subscribe(data => {
      this.domainData=data;
      console.log(this.domainData)    }, err => {
      console.log("error");
    });
  }

  newdomain()
  {
    // this.newDomain = {domainName: "",email:[]};
  this.apiService.getNewDomainData(this.newDomainName).subscribe(data => {
  this.newDomainData=data;
  this.domainData.push(this.newDomainData)
  this.toastr.successToastr('Tracking Domain '.concat(this.newDomainName), 'Success!');
  
   }, err => {
  console.log("error");

  this.toastr.errorToastr('Unable to pull Records for the domain'.concat(this.newDomainName), 'Oops!');
  // this.toastr.error("Network Issue Try agin after some time", "Oops")
});
this.newDomainName=''

    this.newdomainTrigger=false

  }

}
