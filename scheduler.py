import subprocess
import json
from jinja2 import *
from datetime import datetime
from modules.domain import queryDomain
from modules.ssl_info import queryssl
from modules.mailer import send_mail
from modules.db_handler import putjsonForId,getjsonForId,getAsJson,Updatedata

def check_expiry(json_data):
	receivers=[]
	check_days=[30,15,7]
	for domain in json_data:
		domainName=domain['domainName']
		domain_remaining_days=domain['domain_remaining_days']
		domain_expiry_date=domain['domain_expiry_date']
		ssl_expiry_days=domain['ssl_expiry_days']
		ssl_expiry_date=domain['ssl_expiry_date']
		if int(domain['domain_remaining_days']) in check_days :
			obj={}
			obj['email']=domain['email']
			obj['domain']=domain['domainName']
			obj['trigger']=['domain']
			# domain_expiry_message=f'Your Domain {domainName} is about to expire in {domain_remaining_days} on {domain_expiry_date}'
			# obj['message']=domain_expiry_message
		if int(domain['ssl_expiry_days']) in check_days:
			try:	
				obj['trigger'].append('ssl')
				# obj['message']=f'Your Domain {domainName} is about to expire in {domain_remaining_days} on {domain_expiry_date}\n also your Certificate for the same domain is about to expire in {ssl_expiry_days}on {ssl_expiry_date}.'
			except NameError:
				obj={}
				obj['email']=domain['email']
				obj['domain']=domain['domainName']
				obj['trigger']=['ssl']
				# obj['message']=f'your Certificate for the {domainName} is about to expire in {ssl_expiry_days}on {ssl_expiry_dates}.'
		try:
			receivers.append(obj)
		except NameError:
			continue
	return receivers
def fetch_data():
	json_data=getAsJson()
	for domain in json_data:
		days,expiry_date,registrar=queryDomain(domain['domainName'])
		domain['domain_remaining_days'],domain['domain_expiry_date'],domain['domain_registrar']=days,expiry_date,registrar
		days,expiry_date,ssl_issuer=queryssl(domain['domainName'])
		domain['ssl_expiry_days'],domain['ssl_expiry_date'],domain['ssl_issuer']=days,expiry_date,ssl_issuer
	return json_data
def update():
	gather_data=fetch_data()
	check_for_expiry_dates=check_expiry(gather_data)
	if len(check_for_expiry_dates)>0:
		send_mail(check_for_expiry_dates)
	else:
		Updatedata(gather_data)
	write_log=open('log.txt','a')
	message=f"\nscheduler ran at {datetime.now()}"
	write_log.write(message)
	write_log.close()
	print(f"done at {datetime.now()}")
if __name__ =='__main__':
	update()