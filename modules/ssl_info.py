import ssl
from datetime import datetime
import socket
def queryssl(hostname: str) -> datetime:
    ssl_date_fmt = r'%b %d %H:%M:%S %Y %Z'

    context = ssl.create_default_context()
    conn = context.wrap_socket(
        socket.socket(socket.AF_INET),
        server_hostname=hostname,
    )
    # 3 second timeout because Lambda has runtime limitations
    conn.settimeout(5.0)
    conn.connect((hostname, 443))
    ssl_info = conn.getpeercert()
    for i in ssl_info['issuer']:
        for j in i:
            if j[0]=='organizationName':
                ssl_issuer=j[1]
    expiry_date=datetime.strptime(ssl_info['notAfter'],ssl_date_fmt)
    days=expiry_date-datetime.now()
    return days.days,str(datetime.strptime(ssl_info['notAfter'],ssl_date_fmt)).split(' ')[0],ssl_issuer
# queryssl('bing.com')