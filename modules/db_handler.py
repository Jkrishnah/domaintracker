import sqlite3
import json
def putjsonForId(json_data):
	try:
		domain_id=json_data['domainName']
		data=json.dumps(json_data,indent=4)
		get_connection = sqlite3.connect('Domain.db')
		cursor = get_connection.cursor()
		query = """ INSERT INTO domainData(id,data) VALUES(?,?) """
		data_tuple=(domain_id,data)
		cursor.execute(query, data_tuple)
		get_connection.commit()
		cursor.close()
		return True
	except:
		print("Error connecting to DB")
		return False
def getjsonForId(domain_id):
		put_connection=sqlite3.connect('Domain.db')
		cursor = put_connection.cursor()
		query = """select * from domainData where id = ?"""
		cursor.execute(query, (domain_id,))
		records = cursor.fetchall()
		put_connection.commit()
		cursor.close()
		return json.loads(records[0][1])
def getAsJson():
	try:
		get_connection=sqlite3.connect('Domain.db')
		cursor = get_connection.cursor()
		query = """select data from domainData"""
		cursor.execute(query)
		records = cursor.fetchall()
		jsonArray=[json.loads(rec[0]) for rec in records]
		get_connection.commit()
		cursor.close()
		return jsonArray
	except Exception as e:
		print(e)
def Updatedata(json_data):
	try:
		get_connection=sqlite3.connect('Domain.db')
		cursor = get_connection.cursor()
		query = """Update domainData set data = ? where id = ? """
		if type(json_data)==list:
			recordList=[(json.dumps(item),item['domainName']) for item in json_data]
			cursor.executemany(query, recordList)
		else:
			data_tuple=(json.dumps(json_data,indent=4),json_data['domainName'])
			cursor.execute(query, data_tuple)
		get_connection.commit()
		cursor.close()
		return True
	except Exception as e:
		print(e)
		return False
		


