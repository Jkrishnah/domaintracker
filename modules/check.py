from domain import queryDomain
from ssl_info import queryssl
def check(json_data):
	for domain in json_data:
		days,expiry_date,registrar=queryDomain(domain['domainName'])
		domain['domain_remaining_days'],domain['domain_expiry_date'],domain['domain_registrar']=days,expiry_date,registrar
		days,expiry_date=queryssl(domain['domainName'])
		domain['ssl_expiry_days'],domain['ssl_expiry_date']=days,expiry_date
	return json_data