from flask import *
import subprocess
import json
import re
from jinja2 import *
from flask_cors import CORS
from modules.domain import queryDomain
from modules.ssl_info import queryssl
from modules.mailer import send_mail
from modules.db_handler import putjsonForId,getjsonForId,getAsJson,Updatedata
app = Flask(__name__)
CORS(app)
@app.route('/',methods = ['POST', 'GET'])
def index():
   return render_template('index.html')

@app.route('/notifications',methods = ['POST', 'GET'])
def notification():
   return render_template('index.html')

@app.route('/fetchData',methods = ['POST', 'GET'])
def getData():
   return json.dumps(getAsJson())

@app.route('/new_domian',methods = ['POST', 'GET'])
def getNewDomain():
	data=request.json
	response_data=check(data)
	update=putjsonForId(response_data)
	print(update)
	return json.dumps(response_data)
@app.route('/change_email',methods = ['POST', 'GET'])
def UpdateEmail():
	data=request.json
	present_data=getjsonForId(data['domainName'])
	present_data['email']=data['email']
	update=Updatedata(present_data)
	if update:
		return json.dumps({'operation':'success'})
	else:
		return json.dumps({'operation':'failed'})
def check(json_data):
	days,expiry_date,registrar=queryDomain(json_data['domainName'])
	json_data['domain_remaining_days'],json_data['domain_expiry_date'],json_data['domain_registrar']=days,expiry_date,registrar
	days,expiry_date,ssl_issuer=queryssl(json_data['domainName'])
	json_data['ssl_expiry_days'],json_data['ssl_expiry_date'],json_data['ssl_issuer']=days,expiry_date,ssl_issuer
	return json_data 
@app.route('/check_email',methods = ['POST', 'GET'])
def checkmail():
	# try:
		data=request.json
		for domain in data:
			if type(domain['email']) != list:
				if ',' in domain['email']:
					emails=domain['email'].split(',')
					emails=[email.replace(' ','') for email in emails]
					domain['email']=emails
				else:
					email=domain['email'].replace(' ','')
					domain['email']=list()
					domain['email'].append(email)
				present_data=getjsonForId(domain['domainName'])
				present_data['email']=domain['email']
				update=Updatedata(present_data)
			
		return json.dumps({'operation':'success'})
	# except Exception as e:
	# 	print(e)
	# 	return json.dumps({'operation':'failed'})

app.run(debug=True)